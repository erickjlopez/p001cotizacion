const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const app = express();
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyparser.urlencoded({ extended: true }));

let datos = [
	{
		matricula: "2020020309",
		nombre: "ACOSTA ORTEGA JESUS HUMBERTO",
		sexo: "M",
		materias: ["Inglés", "Tecnologías I", "Base de datos"],
	},
	{
		matricula: "2020020001",
		nombre: "ACOSTA VARELA IRVING GUADALUPE",
		sexo: "M",
		materias: ["Inglés", "Tecnologías I", "Base de datos"],
	},
	{
		matricula: "2020020310",
		nombre: "ALMOGAR VAZQUEZ YARLEN DE JESUS",
		sexo: "F",
		materias: ["Inglés", "Tecnologías I", "Base de datos"],
	},
	{
		matricula: "2020030331",
		nombre: "LÓPEZ GONZÁLEZ ERICK JEANICK",
		sexo: "M",
		materias: ["Inglés", "Tecnologías I", "Base de datos"],
	},
	{
		matricula: "2020030219",
		nombre: "LUNA SANDOVAL RUY JESÉ",
		sexo: "M",
		materias: ["Inglés", "Tecnologías I", "Base de datos"],
	},
	{
		matricula: "2020030397",
		nombre: "OSUNA PINEDA CARLOS OMAR",
		sexo: "M",
		materias: ["Inglés", "Tecnologías I", "Base de datos"],
	},
];

app.get("/", (req, res) => {
	//res.send("<h1>Iniciamos con express</h1>");
	res.render("index", { titulo: "LISTADO DE ALUMNOS", listado: datos });
});

app.get("/tablas", (req, res) => {
	//res.send("<h1>Iniciamos con express</h1>");
	const valores = {
		tabla: req.query.tabla,
	};
	res.render("tablas", valores);
});

app.post("/tablas", (req, res) => {
	//res.send("<h1>Iniciamos con express</h1>");
	const valores = {
		tabla: req.body.tabla,
	};
	res.render("tablas", valores);
});

app.get("/cotizacion", (req, res) => {
	//res.send("<h1>Iniciamos con express</h1>");
	const valores = {
		valor: req.query.valor,
		pInicial: req.query.pInicial,
		plazos: req.query.plazos,
		pagoInicial: req.query.pagoInicial,
		totalFin: req.query.totalFin,
		pagoMensual: req.query.pagoMensual,
	};
	res.render("cotizacion", valores);
});

app.post("/cotizacion", (req, res) => {
	//res.send("<h1>Iniciamos con express</h1>");
	const valores = {
		valor: req.body.valor,
		pInicial: req.body.pInicial,
		plazos: req.body.plazos,
		pagoInicial: req.body.pagoInicial,
		totalFin: req.body.totalFin,
		pagoMensual: req.body.pagoMensual,
	};
	res.render("cotizacion", valores);
});

// escuchar el servidor por el puerto 3000
const puerto = 3000;
app.listen(puerto, () => {
	console.log("Iniciado puerto 3000");
});

app.use((req, res, next) => {
	res.status(404).sendFile(__dirname + "/public/error.html");
});
